<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>

<div class="container">

    <section>
        <h1>Create a new game session</h1>

        <form action="#" id="js-createSession">
            <input type="submit" name="create_session" value="Create"/>
        </form>
    </section>

    <section>
        <h1>Join an existing game session</h1>

        <form action="#" id="js-joinForm">
            <input type="text" name="session_code" id="js-sessionCode" placeholder="code"/>
            <input type="submit" value="Join"/>
        </form>
    </section>

    <div id="js-ingame">
        <section>
            <h1>Players</h1>
            <ul id="js-players"></ul>
        </section>

        <section>
            <h1>Events</h1>
            <ol id="js-events"></ol>
        </section>
    </div>

</div>


<!-- Extra: Join sessions from list (localStorage) -->

<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="http://autobahn.s3.amazonaws.com/js/autobahn.min.js"></script>
<script>

    (function() {
        document.getElementById("js-createSession").addEventListener("submit", createSessionHandler);
        document.getElementById("js-joinForm").addEventListener("submit", joinSessionHandler);

        function joinSessionHandler(e) {
            e.preventDefault();
            var sessionCode = document.getElementById("js-sessionCode").value;
            joinSession(sessionCode);
        }

        function createSessionHandler(e) {
            e.preventDefault();

            $.post('/session', function(data) {
                var code = data.code;

                joinSession(code);
            });
        }

        function joinSession(code) {

            // Show that inside session
            // Show leave button
            // Leave should leave session

            var conn = new ab.Session('ws://<?= config('app.wampserver'); ?>:8080',
                function() {
                    conn.subscribe(code, function(topic, data)
                    {
                        var message = JSON.parse(data).message;

                        var li = document.createElement("li");
                        li.innerHTML = message;
                        document.getElementById("js-events").appendChild(li);
                    });
                },
                function() {
                    console.warn('WebSocket connection closed');
                },
                {'skipSubprotocolCheck': true}
            );

        }

    })();
</script>
</body>
</html>
