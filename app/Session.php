<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model {

    public function players() {
        return $this->belongsToMany('App\Player');
    }

}
