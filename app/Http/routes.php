<?php

Route::get('/', function() {
   return View::make('test');
});

// Create a new game session
Route::post('session', 'SessionController@store');

// Get all sessions
Route::get('sessions', 'SessionController@index');

// Get all info from the session
Route::get('sessions/{code}', 'SessionController@show');

// Join a game session
Route::post('sessions/{code}/players', 'SessionController@join');

Route::get('players/{androidId}/sessions', 'SessionController@sessionsByPlayer');

Route::post('players/{androidId}/action', 'PlayerController@action');

// Get all players that are in the game session
Route::get('sessions/{code}/players', 'SessionController@getPlayers');
