<?php namespace App\Http\Controllers;

use App\Http\Requests;

use App\Player;
use App\Session;
use Request;
use ZMQ;

class SessionController extends Controller
{

    /**
     * Create a new session
     *
     * @return Response
     */
    public function store()
    {
        // Generate random 6 digit number
        $randomCode = sprintf("%06d", mt_rand(0, 999999));

        $session = new Session();
        $session->code = $randomCode;
        $session->save();

        return $session;
    }

    public function index()
    {
        return Session::all();
    }

    public function show($code)
    {
        $session = Session::with('players')->where('code', $code)->first();

        if ($session == null) {
            return response("Game session doesn't exist", 400);
        }

        return $session;
    }

    /**
     * Join an existing session
     *
     * @param $code
     * @return Response
     */
    public function join($code)
    {
        $session = Session::where('code', $code)->first();

        if ($session == null) {
            return response("Game session doesn't exist", 400);
        }

        $androidId = Request::get("androidId");

        // Check if the player already exists
        $player = Player::find($androidId);

        if ($player == null) {
            $player = new Player();
            $player->id = $androidId;
        } else if($session->players->contains($player->id)) {
            return response("You have already joined this session", 400);
        }

        $session->players()->save($player);

        $context = new \ZMQContext();
        $socket = $context->getSocket(ZMQ::SOCKET_PUSH, 'my pusher');
        $socket->connect("tcp://localhost:5555");

        $socket->send(json_encode([
            'sessionCode' => $session->code,
            "message" => "Player " . $player->id . " joined the game session",
            "type" => "info",
            "scope" => "global"
        ]));

        // Todo: Return success
        return $player;
    }

    /**
     * @param $androidId
     * @return Response
     */
    public function sessionsByPlayer($androidId)
    {
        return Player::find($androidId)->sessions;
    }

    /**
     * Get players from given session
     *
     * @param $code
     * @return Response
     */
    public function getPlayers($code)
    {
        $session = Session::where('code', $code)->first();

        if ($session == null) {
            return response("Game session doesn't exist", 400);
        }

        return $session->players->all();
    }

}
