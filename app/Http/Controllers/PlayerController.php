<?php namespace App\Http\Controllers;

use Request;
use ZMQ;

class PlayerController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function action($androidId)
    {
        // We are gonna push a new message to correct room, so we need the room also.
        $input = Request::all();

        $context = new \ZMQContext();
        $socket = $context->getSocket(ZMQ::SOCKET_PUSH, 'my pusher');
        $socket->connect("tcp://localhost:5555");

        $data = [
            "sessionCode" => $input['sessionCode'],
            "type" => "info"
        ];

        switch ($input['type']) {

            case 1:
                $data['type'] = "equipment";
                $data['message'] = "Player " . $androidId . " equipped a " . config("cards." . $input['type'] . "." . $input['id'] . ".name");
                break;

            case 2:
                $data['type'] = "usable";
                $data['message'] = "Player " . $androidId . " put something in his pocket secretly";
                break;

            case 3:
                $data['type'] = "tile";
                $data['message'] = "Player " . $androidId . " " . config("cards." . $input['type'] . "." . $input['id'] . ".flavorText");
                break;
        }

        $socket->send(json_encode($data));

        return response()->json(['message' => 'Action has been registered']);
    }

}
